﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{

public class AssetsDefine 
{
	public readonly static string TANK_PREFAB_PATH = "Prefabs/LocalTank";
	public readonly static string PLAYER_PREFAB_PATH = "Prefabs/Tank";

	public readonly static string SHELL_PREFAB_PATH = "Prefabs/Shell";                    // 炮弹
    public readonly static string WEAK_SHELL_PREFAB_PATH = "Prefabs/WeakShell";           // 轻量级炮弹

	public readonly static string TANK_EXPLOSION_PATH = "Prefabs/TankExplosion"; // 坦克爆炸特效


	public readonly static string OFFLINE_ENTITY_CONFIG = "Configs/offline_entity";      // 离线entity配置
}

}   // end namespace Game