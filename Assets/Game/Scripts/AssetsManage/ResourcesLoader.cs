using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using GameCommon;

namespace Game
{


public class ResourcesLoader : MonoSingleton<ResourcesLoader> 
{
    private Dictionary<string, GameObject> m_LoadedPrefabs = new Dictionary<string,GameObject>();

    public GameObject Load(string p_path)
    {
        GameObject prefab = GetLoadedPrefab(p_path);
        if (prefab != null)
            return prefab;

        prefab = Resources.Load(p_path) as GameObject;
        if(prefab == null)
        {
            GameLogger.Error("ResourcesLoader::Load:cant load resource {0}", p_path);
            return null;
        }

        m_LoadedPrefabs.Add(p_path, prefab);
        return prefab;
    }

    public void LoadAsync(string p_path, Action<GameObject> p_callback)
    {
        StartCoroutine(AsyncLoader(p_path, delegate(GameObject p_gameObject) 
                                        {
                                            p_callback(p_gameObject);
                                        }
                                        ));
    }

    private IEnumerator AsyncLoader(string p_path, Action<GameObject> p_callback)
    {
        GameObject prefab = GetLoadedPrefab(p_path);
        if(prefab != null)
        {
            yield return null;  // 等待一帧后再返回，不知道直接返回是否会有问题
        }
        else
        {
            ResourceRequest request = Resources.LoadAsync(p_path);
            yield return request;

            prefab = request.asset as GameObject;
            if (prefab == null)
                GameLogger.Error("ResourcesLoader::AsyncLoader:cant load resource {0}", p_path);
            else
                m_LoadedPrefabs.Add(p_path, prefab);
        }

        if (p_callback != null)
            p_callback(prefab);
    }

    private GameObject GetLoadedPrefab(string p_path)
    {
        GameObject prefab;
        m_LoadedPrefabs.TryGetValue(p_path, out prefab);
        return prefab;
    }

    public void ClearCathe()
    {
        GameLogger.Log("ResourcesLoader::ClearCathe:" + m_LoadedPrefabs.Keys.ToString());
        m_LoadedPrefabs.Clear();
    }
}

}   // end namespace Game