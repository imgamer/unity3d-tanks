﻿using System.Collections.Generic;
using UnityEngine;

using GameCommon;

namespace Game
{

public enum CameraType
{
    STATIC,
    PLAYER,
    ALL_ENTITY
}

public class CameraControl : MonoBehaviour
{
    public float m_DampTime = 0.2f;                 
    public float m_ScreenEdgeBuffer = 4f;           
    public float m_MinSize = 6.5f;

    // for debug,显示相机看的位置
    private GameObject m_DebugLook;

    private Camera m_Camera;                        
    private float m_ZoomSpeed;                      
    private Vector3 m_MoveVelocity;                 
    private Vector3 m_DesiredPosition;

    private static CameraControl m_Instance;

    private CameraType m_Type = CameraType.STATIC;     // 设置相机状态

    public static CameraControl Instance
    {
        get
        {
            return m_Instance;
        }
    }

    private void Awake()
    {
        if(m_Instance == null)
        {
            m_Instance = this;
            m_Instance.Init();
        }
        else if(m_Instance != this)
        {
            Debug.LogError("CameraControl::Awake:duplicate Main instance, now destoy the newer one.");
            Destroy(this.gameObject);
        }
    }

    private void Init()
    {
        m_Camera = GetComponentInChildren<Camera>();

        m_DebugLook = Instantiate(ResourcesLoader.Instance.Load("Prefabs/CameraLook"));
    }

    public void SetCameraType(CameraType p_state)
    {
        m_Type = p_state;
    }

    private void FixedUpdate()
    {
        if (m_Type == CameraType.STATIC)
            return;

        Move();
        Zoom();
    }


    private void Move()
    {
        FindAveragePosition();

        transform.position = Vector3.SmoothDamp(transform.position, m_DesiredPosition, ref m_MoveVelocity, m_DampTime);
    }

    private List<SceneEntity> GetCurrentEntities()
    {
        if (m_Type == CameraType.PLAYER)
        {
            BaseScene scene = WorldManager.Instance.CurrentScene as BaseScene;
            return scene.Players;
        }
        else if (m_Type == CameraType.ALL_ENTITY)
            return new List<SceneEntity>(WorldManager.Instance.CurrentScene.SceneEntities);
        else
        {
            GameLogger.Error("CameraControl::GetCurrentEntities:wrong CameraType:" + m_Type.ToString());
            return null;
        }
    }

    private void FindAveragePosition()
    {
        Vector3 averagePos = new Vector3();
        int numTargets = 0;

        foreach (SceneEntity sceneEntity in GetCurrentEntities())
        {
            if (!sceneEntity.gameObject.activeSelf)
                continue;

            averagePos += sceneEntity.transform.position;
            numTargets++;
        }

            //for (int i = 0; i < m_Targets.Length; i++)
            //{
            //    if (!m_Targets[i].gameObject.activeSelf)
            //        continue;

            //    averagePos += m_Targets[i].position;
            //    numTargets++;
            //}

        if (numTargets > 0)
            averagePos /= numTargets;

        averagePos.y = transform.position.y;

        m_DesiredPosition = averagePos;

        m_DebugLook.transform.position = m_DesiredPosition;
    }


    private void Zoom()
    {
        float requiredSize = FindRequiredSize();
        m_Camera.orthographicSize = Mathf.SmoothDamp(m_Camera.orthographicSize, requiredSize, ref m_ZoomSpeed, m_DampTime);
    }


    private float FindRequiredSize()
    {
        Vector3 desiredLocalPos = transform.InverseTransformPoint(m_DesiredPosition);

        float size = 0f;

        foreach (SceneEntity sceneEntity in GetCurrentEntities())
        {
            if (!sceneEntity.gameObject.activeSelf)
                continue;

            Vector3 targetLocalPos = transform.InverseTransformPoint(sceneEntity.transform.position);

            Vector3 desiredPosToTarget = targetLocalPos - desiredLocalPos;

            size = Mathf.Max (size, Mathf.Abs (desiredPosToTarget.y));

            size = Mathf.Max (size, Mathf.Abs (desiredPosToTarget.x) / m_Camera.aspect);
        }
        
        size += m_ScreenEdgeBuffer;

        size = Mathf.Max(size, m_MinSize);

        return size;
    }


    public void SetStartPositionAndSize()
    {
        FindAveragePosition();

        transform.position = m_DesiredPosition;

        m_Camera.orthographicSize = FindRequiredSize();
    }
}

}   // end namespace Game