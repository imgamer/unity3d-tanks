﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicPoint : MonoBehaviour {

    private static Transform m_basicTransform = null;

	void Awake () 
    {
        if (m_basicTransform != null)
        {
            Debug.LogError("BasicPoint::Awake:there is already a BasicPoint,destroy self.");
            Destroy(this);
        }
        m_basicTransform = this.transform;
	}

    public static Transform GetTransfrom()
    {
        if (m_basicTransform == null)
            Debug.LogError("BasicPoint::GetTransfrom:m_basicTransform is null.");

        return m_basicTransform;
    }
}
