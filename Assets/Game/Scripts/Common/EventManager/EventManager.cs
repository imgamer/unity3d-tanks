using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{

// 外部以这个类型来注册监听事件
// 触发事件时只能传一个参数，对于多参数需求，可以是定义struct事件参数，注册方和触发方约定好
// 也可考虑考虑可变参数列表，默认为空，需要时使用
public delegate void EventCallback<T>(T p_arg);

/// <summary>
/// 观察者模式实现的事件管理系统
/// </summary>
public static class EventManager
{
    private static Dictionary<EventID, Delegate> m_events = new Dictionary<EventID, Delegate>();

    /// <summary>
    /// 注册监听事件
    /// </summary>
    /// <typeparam name="T">对于每一个p_eventID，接收的委托函数类型是确定的T，注册方和触发方约定好的类型</typeparam>
    /// <param name="p_eventID">事件ID</param>
    /// <param name="p_callback">回调函数</param>
    public static void Register<T>(EventID p_eventID, EventCallback<T> p_callback)
    {
        lock(m_events)
        {
            if(!m_events.ContainsKey(p_eventID))
            {
                m_events.Add(p_eventID, p_callback);
            }
            else
            {
                m_events[p_eventID] = (EventCallback<T>)m_events[p_eventID] + p_callback; 
            }
        }
    }

    public static void Unregister<T>(EventID p_eventID, EventCallback<T> p_callback)
    {
        lock(m_events)
        {
            if(m_events.ContainsKey(p_eventID))
            {
                m_events[p_eventID] = (EventCallback<T>)m_events[p_eventID] - p_callback;
            }

            if (m_events[p_eventID] == null)
                m_events.Remove(p_eventID);
        }
    }

    /// <summary>
    /// 触发事件
    /// </summary>
    /// <typeparam name="T">参数类型，触发事件的时候明确的参数类型不需要再装箱拆箱</typeparam>
    /// <param name="p_eventID"></param>
    /// <param name="p_arg"></param>
    public static void Fire<T>(EventID p_eventID, T p_arg)
    {
        Delegate delegateFuc;
        if(m_events.TryGetValue(p_eventID, out delegateFuc))
        {
            EventCallback<T> callback = (EventCallback<T>)delegateFuc;  // 不可直接转换后调用，不能作为表达式语句，需要经过=操作处理成可调用
            try
            {
                callback(p_arg);
            }
            catch(Exception e)
            {
                Debug.LogError("EventManager::Fire:" + p_eventID + "\n" + e.ToString());
            }
        }
    }

    public static void ClearEvents()
    {
        m_events.Clear();
    }
}

}   // end namespace Game