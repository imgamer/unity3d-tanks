using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{

/// <summary>
/// 事件参数模块，事件触发时只能接收一个参数，触发方和接收方可以约定好复杂的参数对象。
/// 统一定义在这个模块中。
/// </summary>

public struct TestParam
{
    int x;
    string s;
    bool result;
}

}   // end namespace Game