﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace GameCommon
{

/// <summary>
/// 提供委托接口的状态，可通过委托灵活创建状态
/// 在创建状态时定义状态行为
/// </summary>
/// <typeparam name="T"></typeparam>
public class DelegateState<T>: State<T>
{
    public event Action OnAttach;
    public event Action OnEnter;
    public event Action OnLeave;
    public event Action<float> OnUpdate;

    public override void Attach(T p_Owner)
    {
        base.Attach(p_Owner);
        if (OnAttach != null)
            OnAttach();
    }

    public override void Enter()
    {
        base.Enter();
        if (OnEnter != null)
            OnEnter();
    }

    public override void Leave()
    {
        base.Leave();
        if (OnLeave != null)
            OnLeave();
    }

    public override void Update(float deltaTime)
    {
        base.Update(deltaTime);
        if (OnUpdate != null)
            OnUpdate(deltaTime);
    }
}

}   // end namespace GameCommon