﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameCommon
{

public abstract class State<T>
{
    protected T m_Owner;

    public virtual void Attach(T p_Owner)
    {
        m_Owner = p_Owner;
    }

    public virtual void Enter()
    { }

    public virtual void Leave()
    { }

    public virtual void Update(float deltaTime)
    { }
}

}   // end namespace Game