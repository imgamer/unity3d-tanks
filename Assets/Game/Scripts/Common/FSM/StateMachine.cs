﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameCommon
{

/// <summary>
/// 有限状态机
/// 设计过程考虑的一些问题如下：
/// 1. State设置为全局Singleton对象还是每个用户拥有的实例。鉴于会存储一些状态过程参数，还是设计为实例，例如寻路状态。
/// 2. state类型是enum还是string。使用了string，这样更通用一些，用户可以管理自己的enum，然后ToString即可。
/// 3. State.Update是否需要带Owner参数，还是State拥有一个Owner参数，AddState时设置。既然附加到用户，那么就直接引用用户吧。
/// 4. State是否需要提供OnEnter等委托方法，以便外部扩展。委托方法存在会让Enter等接口作用不大，但是灵活度很高。
/// </summary>
/// <typeparam name="T"></typeparam>
public class StateMachine<T>
{
    // 状态类型是string，用户自己管理，可以是enum.ToString();
    private Dictionary<string, State<T>> m_States = new Dictionary<string, State<T>>();

    private State<T> m_CurrentState = null;

    private T m_Owner;

    public StateMachine(T p_Owner)
    {
        m_Owner = p_Owner;
    }

    public State<T> CurrentState
    {
        get { return m_CurrentState; }
        set { m_CurrentState = value; }
    }

    public void AddState(string p_stateType, State<T> p_state)
    {
        if (HasState(p_stateType))
        {
            GameLogger.Log("StateMachine::AddState:there is a same state({0}) in FSM.", p_stateType.ToString());
            return;
        }

        m_States.Add(p_stateType, p_state);
        p_state.Attach(m_Owner);
    }

    public void Update(float deltaTime)
    {
        if(CurrentState != null)
            CurrentState.Update(deltaTime);
    }

    public void ChangeState(string p_stateType)
    {
        if (!HasState(p_stateType))
        {
            GameLogger.Log("StateMachine::ChangeState:there is no state({0}) in fms.", p_stateType.ToString());
            return;
        }

        if(CurrentState != null)
        {
            CurrentState.Leave();
        }
        CurrentState = m_States[p_stateType];
        CurrentState.Enter();
    }

    private bool HasState(string p_stateType)
    {
        return m_States.ContainsKey(p_stateType);
    }
}

}   // end namespace GameCommon