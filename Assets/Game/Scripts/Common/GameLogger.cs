﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameCommon
{

public static class GameLogger 
{

    public static void Log(string p_str, params object[] p_args)
    {
        Debug.Log("[DEBUG]:" + string.Format(p_str, p_args));
    }

    public static void Warning(string p_str, params object[] p_args)
    {
        Debug.LogWarning("[WARNING]:" + string.Format(p_str, p_args));
    }

    public static void Error(string p_str, params object[] p_args)
    {
        Debug.LogError("[ERROR]:" + string.Format(p_str, p_args));
    }
}

}   // end namespace Game