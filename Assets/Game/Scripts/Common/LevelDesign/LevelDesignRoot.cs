using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{

/// <summary>
/// 关卡根节点，关卡流程入口
/// </summary>
public class LevelDesignRoot : LevelElementQueue
{
    public bool m_ActiveOnStart = true;

    void Awake()
    {
        this.Init();
    }

	// Use this for initialization
	void Start () 
    {
        TryActive();
	}

    protected override void OnPass()
    {
 	    base.OnPass();

        // 所有关卡结束
    }

    protected override void OnReset()
    {
        base.OnReset();

        TryActive();
    }

    private void TryActive()
    {
        if (m_ActiveOnStart)
            Active();
    }

    public override void OnDrawGizmos()
    {
#if UNITY_EDITOR
            base.OnDrawGizmos();

            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, 0.2f);
#endif
    }
}

}   // end namespace Game
