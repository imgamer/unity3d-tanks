﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{

/// <summary>
/// 关卡元素的职责是：关卡激活、关卡通过、关卡失败（没有通过即失败）、通知父关卡、管理子关卡
/// 关卡元素的状态：是否激活、是否已通关、其它
/// 关卡元素可以互为父子，嵌套使用
/// </summary>
public abstract class LevelElement : MonoBehaviour 
{
    private LevelElement m_parent = null;
    public LevelElement Parent
    {
        get { return m_parent; }
        set { m_parent = value; }
    }

    public bool IsActive
    {
        get;
        protected set;
    }

    public bool IsPassed
    {
        get;
        protected set;
    }

    public virtual void Init()
    {
        IsActive = false;
        IsPassed = false;
    }

    public void Active()
    {
        if (IsActive)
            throw new System.Exception(this + "::Active(),I had been actived, may be somewhere has logic error.");

        IsActive = true;

        // 如果未激活状态才激活。场景元素的功能实现不应该依赖unity api
        if(!gameObject.activeSelf)
           gameObject.SetActive(true); 

        OnActive();     // 使用自定义的激活处理，而不是OnEnable
    }

    protected virtual void OnActive()
    { }

    public void Pass()
    {
        if(IsPassed)
            throw new System.Exception(this + "::Pass(),I had been passed, may be somewhere has logic error.");

        OnPass();
        IsActive = false;
        IsPassed = true;

        if (Parent != null)
            Parent.ChildPassed(this);
    }

    protected virtual void OnPass()
    { }

    // 关卡重置，重新开始关卡流程
    public void Reset()
    {
        IsActive = false;
        IsPassed = false;

        OnReset();
    }

    // 子类重置时做点事
    protected virtual void OnReset()
    {
    }

    /// <summary>
    /// 一个关卡元素可能有父关卡和子关卡
    /// </summary>
    /// <param name="p_child"></param>
    public virtual void ChildPassed(LevelElement p_child)
    { }

    public virtual void OnDrawGizmos()
    {
#if UNITY_EDITOR
        if (transform.parent)
        {
            Gizmos.color = Color.white;
            Gizmos.DrawLine(transform.position, transform.parent.position);

            //TextMesh text = transform.GetComponentInChildren<TextMesh>();
            ////if (!text)
            ////{
            ////    text = this.gameObject.AddComponent<TextMesh>();
            ////    text.text = "出生点";
            ////    text.characterSize = 0.25f;
            ////    text.anchor = TextAnchor.LowerCenter;
            ////}
            //Camera targetCamera = UnityEditor.SceneView.lastActiveSceneView.camera;
            //if (targetCamera != null && text != null)
            //{
            //    text.transform.rotation = targetCamera.transform.rotation;

            //    float distance = Vector3.Distance(transform.position, targetCamera.transform.position);
            //    float scale = distance / 8;
            //    text.transform.localScale = new Vector3(scale, scale, 1);
            //}
        }
#endif
    }
}

}   // end namespace Game