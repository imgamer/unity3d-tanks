﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Game
{

/// <summary>
/// 维护多个对象，对象会在ElemGroup被激活时一次性全部激活
/// </summary>
public class LevelElementGroup : LevelElement 
{
    private List<LevelElement> m_Elements = new List<LevelElement>();
    private int m_elementCount = 0;

    public override void Init()
    {
        base.Init();
        foreach(Transform childTransform in transform)
        {
            LevelElement element = childTransform.GetComponent<LevelElement>();
            if(element != null)
            {
                element.Parent = this;
                element.Init();
                m_Elements.Add(element);
                m_elementCount += 1;
            }
        }
    }

    protected override void OnActive()
    {
        base.OnActive();

        if(m_Elements.Count == 0)
        {
            Pass();
        }
        else
        {
            foreach(LevelElement element in m_Elements)
            {
                element.Active();
            }
        }
    }

    public override void ChildPassed(LevelElement p_child)
    {
        base.ChildPassed(p_child);

        m_elementCount -= 1;
        if (m_elementCount == 0)
            Pass();
    }

    protected override void OnReset()
    {
        base.OnReset();

        m_elementCount = 0;
        foreach(LevelElement element in m_Elements)
        {
            element.Reset();
            m_elementCount += 1;
        }
    }

    public override void OnDrawGizmos()
    {
#if UNITY_EDITOR
        base.OnDrawGizmos();

        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, 0.2f);
#endif
    }
}

}   // end namespace Game