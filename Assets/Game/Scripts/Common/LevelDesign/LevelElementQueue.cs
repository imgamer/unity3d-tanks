﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using GameCommon;

namespace Game
{

/// <summary>
/// 封装LevelElement元素以便具有比较功能，LevelElementQueue需要顺序排列的元素逐个激活
/// </summary>
class ListLevelElement : IComparer<ListLevelElement>
{
    public int Index
    { get; set; }

    public LevelElement Element
    { get; set; }

    public int Compare(ListLevelElement p_ElementA, ListLevelElement p_ElementB)
    {
        if (p_ElementA.Index > p_ElementB.Index)
            return 1;
        else if (p_ElementA.Index < p_ElementB.Index)
            return -1;
        return 0;
    }

    public int CompareTo(ListLevelElement p_Element)
    {
        return Compare(this, p_Element);
    }
}

/// <summary>
/// 维护多个对象，对象会按顺序激活，同一时间仅会激活一个对象
/// </summary>
public class LevelElementQueue : LevelElement
{
    public float m_SwitchDelay = 0f;        // 关卡切换延时
    private Coroutine m_Coroutine;          // 关卡重置时需要退出Coroutine，因此需要存储

    private int m_Index = 0;
    private LevelElement m_CurrentElement = null;
    private List<LevelElement> m_Elements = new List<LevelElement>();

    public override void Init()
    {
        base.Init();

        foreach(Transform childTransform in transform)
        {
            LevelElement element = childTransform.GetComponent<LevelElement>();
            if(element != null)
            {
                element.Parent = this;
                m_Elements.Add(element);
                element.Init();
            }
        }
    }

    protected override void OnActive()
    {
        base.OnActive();

        if(m_Elements.Count > m_Index)
        {
            m_CurrentElement = m_Elements[m_Index];
            m_CurrentElement.Active();
        }
        else
        {
            Pass();
        }
    }

    public override void ChildPassed(LevelElement p_child)
    {
        base.ChildPassed(p_child);

        if (p_child != m_CurrentElement)
            throw new System.Exception("LevelElementQueue::ChildPassed:child not current element, may be somewhere has logic error.");

        ++m_Index;
        if(m_Elements.Count > m_Index)
        {
            m_CurrentElement = m_Elements[m_Index];
            if(m_SwitchDelay > 0f)
            {
                m_Coroutine = StartCoroutine(DelayActive());
            }
            else
            {
                m_CurrentElement.Active();
            }
        }
        else
        {
            Pass();
        }
    }

    private IEnumerator DelayActive()
    {
        yield return new WaitForSeconds(m_SwitchDelay);
        m_Coroutine = null;

        m_CurrentElement.Active();
    }

    private void CancelCoroutine()
    {
        if (m_Coroutine != null)
        {
            GameLogger.Log("LevelElementQueue::CancelCoroutine:" + m_Coroutine.ToString());

            StopCoroutine(m_Coroutine);
            m_Coroutine = null;
        }
    }

    protected override void OnReset()
    {
        base.OnReset();

        m_Index = 0;
        m_CurrentElement = null;

        CancelCoroutine();

        foreach(LevelElement element in m_Elements)
        {
            element.Reset();
        }
    }

    public override void OnDrawGizmos()
    {
#if UNITY_EDITOR
        base.OnDrawGizmos();

        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, 0.2f);
#endif
    }
}

}   // end namespace Game