﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace GameCommon
{

// MonoSingleton是拥有unity 组件功能的Singleton对象，需要挂到对象上
// 需要解决的问题是，Singleton防御，保证只会有唯一的对象和组件
// 对于手动挂到对象上的组件创建时会绕开Create流程，因此所有的MonoSingleton对象都是在代码里动态创建的
public class MonoSingleton<T> : MonoBehaviour where T : MonoSingleton<T>
{
    private static readonly string m_SingletonName = "Singleton";

    private static GameObject m_GameObject = null;
    private static T m_Instance;

    public static T Instance
    {
        get
        {
            if (m_Instance == null)
                Create();

            return m_Instance;
        }
    }

    // 防御其它途径创建的对象，例如直接把脚本挂在GameObject上。务必保证经过Create流程。
    void Awake()
    {
        // Awake会在动态添加Singleton组件时触发
        // 添加MonoSingleton组件之前必须已经经过Create流程创建了全局对象
        Debug.Assert(m_GameObject != null);

        if (this.gameObject != m_GameObject)
        {
            Debug.LogError(string.Format("MonoSingleton::Awake:must be created from Create function, destroy gameObject({0}).", this.gameObject.name));
            Object.Destroy(this.gameObject);
            return;
        }
        // m_Instance在Create时初始化，第一次进来是null
        if (m_Instance !=null && this != m_Instance)
        {
            Debug.LogError(string.Format("MonoSingleton::Awake:{0} must be a Singleton component, destroy the duplicate one.", typeof(T).Name));
            Object.Destroy(this);
            return;
        }
    }

    public static void Create()
    {
        if (m_Instance == null)
        {
            if(m_GameObject == null)
            {
                CreateRoot();
            }

            m_Instance = m_GameObject.AddComponent<T>();   // 会先触发Awake然后再赋值
            m_Instance.Init();
        }
    }

    // Destroy会在销毁时被调用，会覆盖基类中的Destroy吗
    void OnDestroy()
    {
        if (m_Instance != null)
            m_Instance.Finish();
    }

    private static void CreateRoot()
    {
        if(m_GameObject == null)
        {
            m_GameObject = new GameObject(m_SingletonName);
            Object.DontDestroyOnLoad(m_GameObject);
        }
    }

    private void Init()
    {
        OnInit();
    }

    protected virtual void OnInit()
    { }

    private void Finish()
    {
        OnFinish();
    }

    protected void OnFinish()
    { }
}

}   // end namespace GameCommon.