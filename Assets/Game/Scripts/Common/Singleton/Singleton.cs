﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameCommon
{

public abstract class Singleton<T> where T : Singleton<T>, new()
{
    private static T m_instance;

    public static T Instance
    {
        get
        {
            if (m_instance == null)
                Create();
            return m_instance;
        }
    }

	protected Singleton()
	{
		Debug.Assert (m_instance == null);

		m_instance = (T)this;
		m_instance.Init();
	}

    public static void Create()
    {
        if (m_instance != null)
        {
            Debug.LogError(string.Format("Singleton<{0}>::Create:there is already a instance.", m_instance));
            return;
        }

        new T();
        
    }

    private void Init()
    {
        OnInit();
    }

    protected virtual void OnInit()
    { }

}

}   // end namespace GameCommon