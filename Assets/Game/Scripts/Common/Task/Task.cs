﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameCommon
{

// Task相关委托的定义
public delegate void TaskInit();                         // 任务初始化
public delegate void TaskShutdown();                     // 任务关闭
public delegate void TaskUpdate(float p_deltaTime);      // 任务每帧更新
public delegate bool TaskIsOver();                       // 任务是否结束

/// <summary>
/// 封装了一些行为，在管理器中每一帧尝试执行这些行为，直到触发结束条件
/// 执行完毕后会有回调处理
/// </summary>
public class Task
{
    // 这里委托成员变量声明不能使用事件，因为外部常常需要重新创建委托成员变量
    // 这些委托成员变量的使用会违反封装的一些原则，但这是正确的
    public TaskInit Init = delegate() { };
    public TaskShutdown Shutdown = delegate() { };
    public TaskUpdate Update = delegate(float deltaTime) { };
    public TaskIsOver IsOver = delegate() { return true; };

}

}   // end namespace GameCommon