﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameCommon
{

/// <summary>
/// Task组，完成条件是全部子Task执行完毕
/// </summary>
class TaskGroup: Task
{
    protected List<Task> m_Tasks = new List<Task>();    // 子Task列表
    protected int m_CompleteCount = 0;                  // 已完成的子Task数量

    protected bool m_Running = false;   // 是否已经开始运行，有可能在运行过程中加入新的子Task

    public TaskGroup()
    {
        foreach(Task task in m_Tasks)
        {
            PackShutdown(task);
        }

        // Init会在TaskGroup请求TaskManager执行时被调用
        this.Init = delegate()
        {
            for (int i = 0; i < m_Tasks.Count; ++i)
            {
                TaskManager.Instance.AddTask(m_Tasks[i]);
            }

            m_Running = true;
        };

        this.IsOver = delegate()
        {
            bool isOver = base.IsOver();
            return isOver && m_CompleteCount == m_Tasks.Count;
        };
    }

    public void AddTask(Task p_task)
    {
        m_Tasks.Add(p_task);

        PackShutdown(p_task);

        if(m_Running)
            TaskManager.Instance.AddTask(p_task);
    }

    protected void PackShutdown(Task p_task)
    {
        TaskShutdown shutdown = p_task.Shutdown;
        p_task.Shutdown = delegate()
        {
            shutdown();
            this.m_CompleteCount += 1;
        };
    }
}

}   // end namespace GameCommon