﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameCommon
{

/// <summary>
/// Task管理器，会放置在场景中
/// </summary>
class TaskManager: MonoBehaviour
{
    // 如果任务列表超过这个常量，就需要检查列表是否浪费了空间
    private const int TASK_LIST_CHECK_CAPACITY = 50;

    private static TaskManager m_TaskManager;
    public static TaskManager Instance
    {
        get { return TaskManager.m_TaskManager; }
    }
    
    private List<Task> m_Tasks = new List<Task>();

    private void Awake()
    {
        if (m_TaskManager == null)
            m_TaskManager = this;
        else if (m_TaskManager != this)
        {
            Debug.LogError("TaskManager::Awake:duplicate Main instance, now destoy the newer one.");
            Destroy(this.gameObject);
        }
    }

    // 这是游戏中的常用机制，因此使用Update来不断尝试执行任务
    // 在Update中执行过多的Task有可能会导致主线程的表现比较卡
    // 后续可以考虑提供Coroutine方式
    private void Update()
    {
        if (m_Tasks.Count == 0)
            return;

        // 从后遍历任务列表，因为遍历过程中会有元素移除
        // 同一个tick中，后加入的Task先执行
        for (int i = m_Tasks.Count - 1; i > 0; ++i)
        {
            Task task = m_Tasks[i];
            task.Update(Time.deltaTime);
            if (task.IsOver())
            {
                task.Shutdown();
                m_Tasks.Remove(task);
            }
        }

        // m_Tasks可能会在运行时不断扩张，需要检查是否需要释放不需要的空间
        if (m_Tasks.Capacity > TASK_LIST_CHECK_CAPACITY && m_Tasks.Count * 2 < m_Tasks.Capacity)
        {
            GameLogger.Warning("TaskManager::Update:m_Tasks wast too much memory.");
            m_Tasks.TrimExcess();
        }
    }

    public void AddTask(Task p_task)
    {
        p_task.Init();
        m_Tasks.Add(p_task);
    }


}


}   // end namespace GameCommon