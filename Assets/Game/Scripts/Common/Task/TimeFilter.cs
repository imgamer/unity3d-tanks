﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{

/// <summary>

/// 设计初衷是用于Task系统，作为Update时间间隔的平滑器
/// Task使用Monobehaviour.Update来更新，帧之间的时间间隔是不稳定的
/// 这个平滑器平均了若干帧内的deltaTime，平滑了每帧的时间
/// </summary>
class TimeFilter
{
    private const int TICK_COUNT = 15;

    private float[] m_Ticks = new float[TICK_COUNT];
    private int m_Index = 0;

    public TimeFilter()
    {
        for(int i = 0;i < TICK_COUNT; ++i)
            m_Ticks[i] = 0.015f;    // 随便给的一个初始值，使用过程中如有问题再调整
    }

    public float Interval(float p_deltaTime)
    {
        m_Ticks[m_Index++] = p_deltaTime;
        if (m_Index == TICK_COUNT)
            m_Index = 0;

        float total = 0.0f;
        for (int i = 0; i < TICK_COUNT; ++i)
            total += m_Ticks[i];

        return total / TICK_COUNT;
    }
}


}   // end namespace Game