﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using GameCommon;

namespace Game
{

// 对应配置
public class OfflineEntityData
{
    public int utype;
    public string model;
    public string modelColor;
    public string name;
    public string script;
}

public class OfflineEntityDataLoader : Singleton<OfflineEntityDataLoader>
{
    private Dictionary<int, OfflineEntityData> m_datas = new Dictionary<int, OfflineEntityData>();
    public Dictionary<int, OfflineEntityData> Datas
    {
        get { return m_datas; }
    }

    protected override void OnInit()
    {
        base.OnInit();

        Load();
    }

    private void Load()
    {
        TextAsset configText = Resources.Load<TextAsset>(AssetsDefine.OFFLINE_ENTITY_CONFIG);
        LitJson.JsonReader reader = new LitJson.JsonReader(configText.text);
        Dictionary<string, OfflineEntityData> datas = LitJson.JsonMapper.ToObject<Dictionary<string, OfflineEntityData>>(reader);

        foreach(OfflineEntityData data in datas.Values)
        {
            Datas.Add(data.utype, data);
        }
    }

    public OfflineEntityData GetData(int p_utype)
    {
        OfflineEntityData data;
        if (!Datas.TryGetValue(p_utype, out data))
            GameLogger.Error("OfflineEntityDataLoader::GetData:cant get data:" + p_utype);

        return data;
    }
}

}   // end namespace Game