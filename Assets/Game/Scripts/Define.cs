﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{

class Define
{
    // 场景脚本名字定义，可通过名字反射得到对应的场景管理类型
    public const string SCENE_SCRIPT_NAME_LOGIN = "LoginScene";
}


}   // end namespace Game