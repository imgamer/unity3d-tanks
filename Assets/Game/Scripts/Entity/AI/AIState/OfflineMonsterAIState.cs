﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using GameCommon;

namespace Game
{

public enum OfflineMonsterAIState
{
    Fire,
    Search,
    Trace,
    Attack
}

/// <summary>
/// 考虑给地图设置多个区域，monster逐个区域搜索玩家，看起来就不会那么傻
/// </summary>
class StateSearchTarget: DelegateState<OfflineSceneMonster>
{

    public StateSearchTarget()
    {
        this.OnUpdate += delegate(float p_deltaTime)
        {
            OfflineSceneAvatar enemy = FindEnemy();
            if (enemy != null)
            {
                m_Owner.Target = enemy;
                StopSearch();
                m_Owner.ChangeAIState(OfflineMonsterAIState.Fire);
                return;
            }

            StartSearch();
        };
    }

    private void StartSearch()
    {
        Vector3 destination = FindRandomDestination();
        // 找不到则随机移动，规则是玩家周围随机某个点
        m_Owner.MoveToPosition(destination);
    }

    private void StopSearch()
    {
        m_Owner.StopMove();
    }

    private OfflineSceneAvatar FindEnemy()
    {
        OfflineScenePlayer player = SceneEntityManager.Instance.OfflinePlayer;
        if (player.IsDead())
            return null;

        if (Vector3.Distance(m_Owner.transform.position, player.transform.position) <= m_Owner.m_AlertRadius)
            return player;
        else
            return null;

        //List<SceneOfflineAvatar> avatars = SceneOfflineAvatar.OfflineAvatarInRange(m_Owner, m_Owner.m_AlertRadius);
        //if(avatars.Count > 0)
        //    return avatars[0];
        //else
        //    return null;
    }

    private Vector3 FindRandomDestination()
    {
        OfflineScenePlayer player = SceneEntityManager.Instance.OfflinePlayer;

        Vector3 center = player.transform.position;
        float radius = Random.Range(m_Owner.m_FireRadius, m_Owner.m_AlertRadius);
        float angle = 360.0f * Random.value;
        float x = radius * Mathf.Cos(angle);
        float y = radius * Mathf.Sin(angle);
        return new Vector3(center.x + x, center.y, center.z + y);
    }
}

/// <summary>
/// 如果敌人进入警戒范围，那么攻击敌人，如果敌人超过开火范围，那么追踪敌人并开火
/// 如果敌人超出了警戒范围，那么切换到搜索状态
/// </summary>
class AIStateTrace: DelegateState<OfflineSceneMonster>
{
    public AIStateTrace()
    {
        this.OnAttach += delegate()
        {

        };

        this.OnUpdate += delegate(float deltaTime)
        {
            if (m_Owner.Target == null)
            {
                m_Owner.ChangeAIState(OfflineMonsterAIState.Search);
                return;
            }
        };
    }
}

class AIStateAttack: DelegateState<OfflineSceneMonster>
{

}

class AIStateFire : DelegateState<OfflineSceneMonster>
{
    OfflineMonsterMovement m_Movement;

    public AIStateFire()
    {
        this.OnAttach += delegate()
        {
            m_Movement = m_Owner.GetComponent<OfflineMonsterMovement>();
        };

        this.OnEnter += delegate()
        {
            m_Owner.LookAtTarget();
            m_Owner.Fire();
        };

        this.OnUpdate += delegate(float deltaTime)
        {
            if (m_Owner.Target == null || m_Owner.Target.IsDead() || 
                Vector3.Distance(m_Owner.Target.transform.position, m_Owner.transform.position) >= m_Owner.m_AlertRadius)
            {
                m_Owner.Target = null;
                m_Owner.ChangeAIState(OfflineMonsterAIState.Search);
                return;
            }

            if(m_Movement.Angle(m_Owner.Target.transform.position) > 30.0f)
            {
                m_Owner.LookAtTarget();
            }

            m_Owner.Fire();
        };
    }

}

}   // end namespace Game