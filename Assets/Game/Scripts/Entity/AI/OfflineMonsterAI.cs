﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using GameCommon;

namespace Game
{

/// <summary>
/// 临时AI需求：
/// 坦克出生后的任务是搜索敌人并消灭，进入搜索状态
/// a. 如果在目标攻击范围，那么直接攻击
/// b. 如果目标在警戒范围，设置目标，跟踪目标（进入跟踪状态）
/// c. 跟踪目标到攻击距离，进行攻击
/// d. 攻击完毕(目标失效，可能是离开或者被消灭)，重新搜索目标
/// e. 没有目标，进入巡逻状态，巡逻的逻辑是在全地图随机移动。这是常规ai，会有事件触发转换到其它ai状态
/// f. 敌人进入警戒范围事件，触发跟踪ai（触发器？）
/// </summary>
[RequireComponent(typeof(OfflineSceneMonster))]
public class OfflineMonsterAI : MonoBehaviour
{
    private StateMachine<OfflineSceneMonster> m_FSM;
    private OfflineSceneMonster m_offlineMonster;

    private float m_ThinkInterval = 0.0f;

    private void Awake()
    {
        m_offlineMonster = GetComponent<OfflineSceneMonster>();

        m_FSM = new StateMachine<OfflineSceneMonster>(m_offlineMonster);
    }

    private void Start()
    {
        string fire = OfflineMonsterAIState.Fire.ToString();
        string search = OfflineMonsterAIState.Search.ToString();
        m_FSM.AddState(fire, FireState());
        m_FSM.AddState(search, SearchState());
        m_FSM.ChangeState(search);
    }

    private void Update()
    {
        if (m_ThinkInterval >= 2)
        {
            m_FSM.Update(m_ThinkInterval);
            m_ThinkInterval = 0.0f;
            return;
        }

        m_ThinkInterval += Time.deltaTime;
    }

    private State<OfflineSceneMonster> FireState()
    {
        return new AIStateFire(); ;
    }

    private State<OfflineSceneMonster> SearchState()
    {
        return new StateSearchTarget();
    }

    public void ChangeState(OfflineMonsterAIState state)
    {
        m_FSM.ChangeState(state.ToString());
    }
}

}   // end namespace Game