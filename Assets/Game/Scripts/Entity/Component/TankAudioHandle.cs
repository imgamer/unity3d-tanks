﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{

public enum MovementAudioType
{
    Idling,
    Driving,
}

public enum ShootingAudioType
{
    Charging,
    Fire,
}

public class TankAudioHandle : MonoBehaviour
{
    [Header("移动音效")]
    public AudioSource m_MovementAudio;       // 移动行为声源
    public AudioClip m_EngineIdlingClip;      // 坦克不动时引擎声音
    public AudioClip m_EngineDrivingClip;     // 坦克开动时引擎声音
    public float m_MovePitchRange = 0.2f;     // pitch（音高），vary(变化，变奏)。发动机噪声的音高变化范围
    public float m_OriginalPitch;             // 开始时场景的声音大小

    [Header("战斗音效")]
    public AudioSource m_ShootingAudio;         // 战斗声源
    public AudioClip m_ChargingClip;            // 炮弹滑膛音效
    public AudioClip m_FireClip;                // 射击音效

    void Awake()
    {

    }

    void Start()
    {
        // NullReferenceException: Object reference not set to an instance of an object
        //m_MovementAudioMap.Add(MovementAudioType.Idling, m_EngineIdlingClip);
        //m_MovementAudioMap.Add(MovementAudioType.Driving, m_EngineDrivingClip);

        //m_ShootingAudioMap.Add(ShootingAudioType.Charging, m_ChargingClip);
        //m_ShootingAudioMap.Add(ShootingAudioType.Fire, m_FireClip);

        m_OriginalPitch = m_MovementAudio.pitch;
    }

    public void PlayMovementAudio(MovementAudioType p_type)
    {
        AudioClip p_clip = p_type == MovementAudioType.Idling ? m_EngineIdlingClip : m_EngineDrivingClip;

        if (m_MovementAudio.clip != p_clip)
        {
            m_MovementAudio.clip = p_clip;
            m_MovementAudio.pitch = Random.Range(m_OriginalPitch - m_MovePitchRange, m_OriginalPitch + m_MovePitchRange);
            m_MovementAudio.Play();
        }
    }

    public void PlayShootingAudio(ShootingAudioType p_type)
    {
        AudioClip clip = p_type == ShootingAudioType.Charging ? m_ChargingClip : m_FireClip;
        m_ShootingAudio.clip = clip;
        m_ShootingAudio.Play();
    }
    
}

}   // end namespace Game