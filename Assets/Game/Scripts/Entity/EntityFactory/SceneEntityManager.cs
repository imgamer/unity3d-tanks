﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using GameCommon;

namespace Game
{

public enum EntityType
{
    Player,
    Monster,
    OfflinePlayer,
    OfflineMonster,
}

/// <summary>
/// 创建SceneEntity的初始化参数
/// </summary>
public struct SceneEntityCreateParam
{
    public int m_id;
    public Color m_color;
}

public class SceneEntityManager : Singleton<SceneEntityManager> 
{
    // 存储封装的离线entity配置数据
    private Dictionary<int, ScriptOfflineEntity> m_ScriptEntities = new Dictionary<int, ScriptOfflineEntity>();

    private int m_CurrentOfflineEntityID = -1;  // 以负数表示离线entity ID

    private SceneEntity m_player;
    public SceneEntity Player
    {
        get { return m_player; }
    }

    public OfflineScenePlayer OfflinePlayer
    {
        get { return m_player as OfflineScenePlayer; } 
    }

    protected override void OnInit()
    {
        base.OnInit();

        InitOfflineEntityData();
    }

    private void InitOfflineEntityData()
    {
        OfflineEntityDataLoader.Create();

        foreach(KeyValuePair<int, OfflineEntityData> pair in OfflineEntityDataLoader.Instance.Datas)
        {
            Type scriptEntity = Type.GetType("Game." + pair.Value.script);
            if (scriptEntity == null)
            {
                GameLogger.Error("SceneEntityManager::InitOfflineEntityData:script({0}) config error.", pair.Key);
                continue;
            }

            // 初始化时如果出错，那么就中断
            ScriptOfflineEntity offlineAvatar = (ScriptOfflineEntity)Activator.CreateInstance(scriptEntity);
            
            offlineAvatar.Init(pair.Value);

            m_ScriptEntities.Add(pair.Key, offlineAvatar);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="p_type">设计目标是通过p_type去读取配置，通过配置里的路径来创建模型实例</param>
    public SceneEntity CreateSceneEntity(EntityType p_type, Vector3 p_position, Quaternion p_rotation, SceneEntityCreateParam p_param)
    {
        SceneEntity sceneEntity = null;

        if (p_type == EntityType.Player)
        {
            GameObject entityPrefab = ResourcesLoader.Instance.Load(AssetsDefine.TANK_PREFAB_PATH);
            GameObject entityGameObject = UnityEngine.Object.Instantiate(entityPrefab, p_position, p_rotation);
            sceneEntity = entityGameObject.AddComponent<ScenePlayer>();
            m_player = sceneEntity as ScenePlayer;
        }
        else if (p_type == EntityType.Monster)
        {
            GameObject entityPrefab = ResourcesLoader.Instance.Load(AssetsDefine.TANK_PREFAB_PATH);
            GameObject entityGameObject = UnityEngine.Object.Instantiate(entityPrefab, p_position, p_rotation);
            sceneEntity = entityGameObject.AddComponent<SceneMonster>();
        }
        else
        {
            GameLogger.Error("SceneEntityManager::CreateSceneEntity:entity type({0}) no define.", p_type.ToString());
            return null;
        }

        //m_currentEntityID += 1;
        //p_param.m_id = m_currentEntityID;
        //sceneEntity.Init(p_param);
        //ScriptSceneManager.Instance.CurrentScene.OnSceneEntityEnter(sceneEntity);
        // sceneEntity.Init()，应该在此处初始化，但目前还没有json配置，配置是外部创建者决定
        return sceneEntity;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="p_utype">以便在编辑器中指定已定义的类型</param>
    /// <param name="p_position"></param>
    /// <param name="p_rotation"></param>
    /// <param name="p_modelColor">离线entity可以在编辑器中编辑模型颜色</param>
    /// <returns></returns>
    public OfflineSceneAvatar CreateOfflineSceneEntity(OfflineEntityUtype p_utype, Vector3 p_position, Quaternion p_rotation, Color p_modelColor)
    {
        int entityID = m_CurrentOfflineEntityID--;
        int utype = (int)p_utype;

        OfflineSceneAvatar sceneEntity = null;
        ScriptOfflineEntity scriptEntity;
        if (m_ScriptEntities.TryGetValue(utype, out scriptEntity))
            sceneEntity = scriptEntity.CreateSceneEntity(p_position, p_rotation, entityID, p_modelColor);
        else
            GameLogger.Error("SceneEntityManager::CreateOfflineSceneEntity:cant find utype:" + p_utype);

        if (p_utype == OfflineEntityUtype.OFFLINE_PLAYER)
            m_player = sceneEntity as SceneEntity;

        sceneEntity.Init();

        // 不能在m_player初始化之前注册，CurrentScene中会使用到m_player，这样的代码难以理解，临时这么处理
        WorldManager.Instance.CurrentScene.OnSceneEntityEnter(sceneEntity);

        return sceneEntity;
    }

    public void CreateSceneEntityAsync(EntityType p_type, Action<SceneEntity> p_callback)
    {
    }

}

}	// end namespace Game