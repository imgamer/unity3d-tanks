﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{

/// <summary>
/// 维护entity的配置数据，提供工厂方法来创建scene entity
/// 配置中配置具体类名，加载时使用类名来创建对应的实例
/// Type scriptClass = Type.GetType("name");Activator.CreateInstance(scriptClass);
/// </summary>
public abstract class ScriptEntity      // 如果有抽象方法，那么类也必须是抽象类
{
    // 工厂方法
    public abstract SceneEntity CreateSceneEntity(Vector3 p_position, Quaternion p_rotation);
}

}   // end namespace Game