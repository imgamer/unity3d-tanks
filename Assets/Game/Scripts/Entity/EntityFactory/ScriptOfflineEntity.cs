﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{

public abstract class ScriptOfflineEntity
{
    protected OfflineEntityData m_data = null;

    public OfflineSceneAvatar CreateSceneEntity(Vector3 p_position, Quaternion p_rotation, int p_id, Color p_modelColor)
    {
        OfflineSceneAvatar sceneEntity = InstantiateSceneEntity(p_position, p_rotation, p_id, p_modelColor);

        sceneEntity.SetID(p_id);

        if (p_modelColor == Color.clear)
        {
            p_modelColor = ColorUtil.HexToColor(m_data.modelColor);
        }
        sceneEntity.SetColor(p_modelColor);

        return sceneEntity;
    }

    public abstract OfflineSceneAvatar InstantiateSceneEntity(Vector3 p_position, Quaternion p_rotation, int p_id, Color p_modelColor);

    public virtual void Init(OfflineEntityData p_data)
    {
        m_data = p_data;
    }
}

}   // end namesapce Game