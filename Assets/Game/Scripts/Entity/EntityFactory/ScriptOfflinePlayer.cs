﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{

public class ScriptOfflinePlayer : ScriptOfflineEntity
{
    public override OfflineSceneAvatar InstantiateSceneEntity(Vector3 p_position, Quaternion p_rotation, int p_id, Color p_modelColor)
    {
        GameObject entityPrefab = ResourcesLoader.Instance.Load(AssetsDefine.TANK_PREFAB_PATH);
        GameObject entityGameObject = UnityEngine.Object.Instantiate(entityPrefab, p_position, p_rotation);

        OfflineSceneAvatar sceneEntity = entityGameObject.AddComponent<OfflineScenePlayer>();

        return sceneEntity;
    }
}

}   // end namesapce Game