﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{

// EntityUtype会是Int32，可以使用enum来定义
public enum OfflineEntityUtype
{
    OFFLINE_PLAYER = 1,              // 离线玩家
    OFFLINE_MONSTER_JIA = 2,         // 怪物甲
    OFFLINE_MONSTER_YI = 3,          // 怪物乙
}

public class UtypeDefine 
{
    public readonly static int OFFLINE_PLAYER = 1;
    public readonly static int OFFLINE_MONSTER_JIA = 2;
    public readonly static int OFFLINE_MONSTER_YI = 3;
}

}   // end namespace Game