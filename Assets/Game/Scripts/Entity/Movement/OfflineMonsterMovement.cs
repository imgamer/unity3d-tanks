﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{

public class OfflineMonsterMovement : MovementController 
{
    private Rigidbody m_Rigidbody;
    private float m_SpeedBias = 0.5f;

    protected void Awake()
    {
        m_Rigidbody = GetComponent<Rigidbody>();
    }

    protected override void StartMoveToPosition()
    {
    }

    private void FixedUpdate()
    {
        if (m_movingParam.moving == false)
            return;

        Turn();
        Move();
    }

    private void Turn()
    {
        // 移动目标与当前朝向相关一定角度时才校正朝向，以避免由于频繁的转向带来的抖动
        if (Angle(m_movingParam.direction) > m_angleForSyncDir)
            LookAt(m_movingParam.position);
    }

    private void Move()
    {
        Vector3 movement = transform.forward * m_SpeedBias * m_movingParam.speed * Time.deltaTime; // 根据输入、速度和帧之间的时间，在坦克面向的方向上创建一个矢量(magnitude)
        m_Rigidbody.MovePosition(m_Rigidbody.position + movement);

        if ((m_movingParam.position - transform.position).magnitude < m_movingParam.stoppingDistance)
            StopMove();
    }

}

}   // end namespace Game
