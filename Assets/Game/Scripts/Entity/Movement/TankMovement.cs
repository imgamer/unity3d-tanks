﻿using UnityEngine;

using UnityStandardAssets.CrossPlatformInput;

namespace Game
{

public class TankMovement : MonoBehaviour
{
    private int m_PlayerNumber = 1;         // 用来指示这是哪一个玩家
    private float m_Speed = 16f;            // 坦克的每秒移动速度，m/s
    private float m_SpeedBias = 0.5f;
    //private float m_TurnSpeed = 180f;       // 坦克的转向速度，degree/s

    private string m_MovementAxisName = "Vertical";     // 控制前后移动输入轴的名字
    private string m_TurnAxisName = "Horizontal";         // 控制转向输入轴的名字
    private Rigidbody m_Rigidbody;         // 用于移动坦克，需要物理碰撞效果
    private float m_MovementInputValue;    // 移动输入的当前值
    private float m_TurnInputValue;        // 转向输入的当前值

	private bool m_moveUpdate = false;

    private TankAudioHandle m_AudioHandle;

    private void Awake()
    {
        m_Rigidbody = GetComponent<Rigidbody>();

        m_AudioHandle = GetComponent<TankAudioHandle>();
    }

    private void OnEnable()
    {
        m_Rigidbody.isKinematic = false;
        m_MovementInputValue = 0f;
        m_TurnInputValue = 0f;
    }


    private void OnDisable ()
    {
        m_Rigidbody.isKinematic = true;
    }

    private void Start()
    {
        m_MovementAxisName = "Vertical" + m_PlayerNumber;
        m_TurnAxisName = "Horizontal" + m_PlayerNumber;
    }

    private void Update()
    {
        m_MovementInputValue = CrossPlatformInputManager.GetAxis(m_MovementAxisName);
        m_TurnInputValue = CrossPlatformInputManager.GetAxis(m_TurnAxisName);

        EngineAudio();
    }

    private void PlayAudioClip(MovementAudioType p_type)
    {
        m_AudioHandle.PlayMovementAudio(p_type);
    }

    private void EngineAudio()
    {
        // Play the correct audio clip based on whether or not the tank is moving and what audio is currently playing.
        if(Mathf.Abs(m_MovementInputValue) < 0.1f || Mathf.Abs(m_TurnInputValue) < 0.1f)
            PlayAudioClip(MovementAudioType.Idling);
        else
            PlayAudioClip(MovementAudioType.Driving);
    }

    private void FixedUpdate()
    {
        // Move and turn the tank.
        Turn();
        Move();
    }


    private void Move()
    {
		if (!m_moveUpdate)
			return;

        // Adjust the position of the tank based on the player's input.
		Vector3 movement = transform.forward * m_SpeedBias * m_Speed * Time.deltaTime; // 根据输入、速度和帧之间的时间，在坦克面向的方向上创建一个矢量(magnitude)
        m_Rigidbody.MovePosition(m_Rigidbody.position + movement);

		m_moveUpdate = false;
    }


    //private void Turn()
    //{
    //    // Adjust the rotation of the tank based on the player's input.
    //    float turn = m_TurnInputValue * m_TurnSpeed * Time.deltaTime;
    //    Quaternion turnRotation = Quaternion.Euler(0.0f, turn, 0.0f);
    //    m_Rigidbody.MoveRotation(m_Rigidbody.rotation * turnRotation);
    //}

    private void Turn()
    {
		float horizontal = m_TurnInputValue;
		float vertical = m_MovementInputValue;
        if (Mathf.Abs(horizontal) > 0.1f || Mathf.Abs(vertical) > 0.1f)
        {
			m_moveUpdate = true;

            float x = horizontal;
            float z = vertical;

            // 限制只能转8个方向
            //if (horizontal != 0)
            //    x = horizontal > 0 ? 1 : -1;
            //if (vertical != 0)
            //    z = vertical > 0 ? 1 : -1;

            // 已经初始化gameobject的朝向和输入轴一致，游戏物体朝向设定平行于xz平面，直接取输入轴的值来生成向量(相对BasicPoint的本地坐标)
            Vector3 inputVector = new Vector3(x, 0, z);

            // 输入轴生成的向量是相对BasicPoint本地坐标而言的，要转成世界坐标
            Vector3 dir = BasicPoint.GetTransfrom().TransformVector(inputVector);

            // 算出BasicPoint与dir向量的夹角
            float angle = Angle360(BasicPoint.GetTransfrom().forward, dir);

            // 当前朝向应该是BasicPoint朝向旋转angle角度，直接设置到朝向。四元数左乘，表示旋转angle角度。
            m_Rigidbody.MoveRotation( Quaternion.Euler(new Vector3(0, angle, 0)) * BasicPoint.GetTransfrom().rotation );
        }
    }

    private float Angle360(Vector3 from, Vector3 to)
    {
        Vector3 v = Vector3.Cross(from, to);
        if (v.y > 0)    // 得到垂直于xz平面的向量
            return Vector3.Angle(from, to);
        else
            return 360 - Vector3.Angle(from, to);
    }
}

}   // end namespace Game