﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{


public class OfflineSceneAvatar : SceneEntity
{
    // 存储所有实例化的SceneOfflineAvatar
    protected static Dictionary<int, OfflineSceneAvatar> m_SceneOfflineAvatars = new Dictionary<int, OfflineSceneAvatar>();

    private GameObject m_CanvasGameObject = null;

    private LevelSpawnPoint m_spawnPoint = null;
    public LevelSpawnPoint SpawnPoint
    {
        get { return m_spawnPoint; }
        set { m_spawnPoint = value; }
    }

    public OfflineSceneAvatar Target
    { get; set; }

    private void Awake()
    {
        m_CanvasGameObject = GetComponentInChildren<Canvas>().gameObject;
    }

    public virtual void Init()
    {
        m_SceneOfflineAvatars.Add(this.ID, this);
    }

	// Use this for initialization
    public override void OnDeath()
    {
        base.OnDeath();

        // 目前死亡只是隐藏自己
        gameObject.SetActive(false);
    }

    public virtual void EnableControl()
    {
        m_CanvasGameObject.SetActive(true);
    }

    public virtual void DisableControl()
    {
        m_CanvasGameObject.SetActive(false);
    }

    public virtual void Reset()
    {
        if (SpawnPoint != null)
        {
            transform.position = SpawnPoint.transform.position;
            transform.rotation = SpawnPoint.transform.rotation;
        }

        // 重新触发OnEnable，TankHealth之类的组件可以重置
        gameObject.SetActive(false);
        gameObject.SetActive(true);
    }

    protected override void LeaveScene()
    {
        base.LeaveScene();

        // 注意：Some objects were not cleaned up when closing the scene. (Did you spawn new GameObjects from OnDestroy?)
        // 不能在OnDestroy时创建新的对象，否则会导致内存泄漏，可能的表现是在编辑器中看到一个不在Hierarchy中的对象
        SpawnPoint.OnSceneEntityDestroy();
    }

    public void SetID(int p_id)
    {
        m_id = p_id;
    }

    public void SetColor(Color p_color)
    {
        ModelColor = p_color;
        base.SetColor();
    }

    protected void OnDestroy()
    {
        m_SceneOfflineAvatars.Remove(this.ID);
    }

    public static List<OfflineSceneAvatar> OfflineAvatarInRange(Vector3 p_centerPoint, float p_radius)
    {
        List<OfflineSceneAvatar> avatars = new List<OfflineSceneAvatar>();
        foreach(OfflineSceneAvatar avatar in m_SceneOfflineAvatars.Values)
        {
            if (Vector3.Distance(p_centerPoint, avatar.transform.position) < p_radius)
                avatars.Add(avatar);
        }
        return avatars;
    }

    public static List<OfflineSceneAvatar> OfflineAvatarInRange(OfflineSceneAvatar p_finder, float p_radius)
    {
        return OfflineAvatarInRange(p_finder.transform.position, p_radius);
    }

    public bool IsDead()
    {
        return gameObject.activeSelf == false;
    }
}


}   // end namespace Game