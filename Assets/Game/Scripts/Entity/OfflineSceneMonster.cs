﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{

public class OfflineSceneMonster : OfflineSceneAvatar 
{
    private OfflineMonsterAI m_AI;
    public float m_AlertRadius = 15.0f;   // 警戒范围
    public float m_FireRadius = 10.0f;     // 开火距离
    private OfflineSceneAvatar m_Target;
    public OfflineSceneAvatar Target
    {
        get { return m_Target; }
        set { m_Target = value; }
    }
    private float m_Speed = 18.0f;      // 移动速度

    private Rigidbody m_Shell;
    private TankCanvas m_TankCanvas;
    private Transform m_FireTransform;
    private TankAudioHandle m_AudioHandle;
    private OfflineMonsterMovement m_Movement;

    public override void Init()
    {
        gameObject.AddComponent<OfflineMonsterMovement>();
        m_Movement = gameObject.AddComponent<OfflineMonsterMovement>();

        GameObject shellPrefab = ResourcesLoader.Instance.Load(AssetsDefine.WEAK_SHELL_PREFAB_PATH);
        m_Shell = shellPrefab.GetComponent<Rigidbody>();

        m_TankCanvas = transform.Find("Canvas").GetComponent<TankCanvas>();
        m_FireTransform = transform.Find("FireTransform");

        m_AudioHandle = GetComponent<TankAudioHandle>();

        m_AI = gameObject.AddComponent<OfflineMonsterAI>(); // ai最后初始化，ai组件有肯能会用到其它组件
    }
    public void ChangeAIState(OfflineMonsterAIState state)
    {
        m_AI.ChangeState(state);
    }

    public override void EnableControl()
    {
        base.EnableControl();
        m_AI.enabled = true;
    }

    public override void DisableControl()
    {
        base.DisableControl();
        m_AI.enabled = false;
    }

    public override void OnDeath()
    {
        base.OnDeath();
    }

    public override void Reset()
    {
        base.Reset();
    }

    public void Fire()
    {
        // Instantiate的对象也可以是个组件，这里m_Shell必须是Rigidbody，否则无法转换为Rigidbody。可了解Instantiate的重载方法
        Rigidbody shellInstance = Instantiate(m_Shell, m_FireTransform.position, m_FireTransform.rotation) as Rigidbody;

        shellInstance.velocity = 15 * m_FireTransform.forward;

        m_AudioHandle.PlayShootingAudio(ShootingAudioType.Fire);

        m_TankCanvas.SetAimSlider(15);
    }

    public void MoveToPosition(Vector3 p_position)
    {
        m_Movement.MoveToPosition(p_position, p_position, m_Speed);
    }

    public void StopMove()
    {
        m_Movement.StopMove();
    }

    public void LookAtTarget()
    {
        if (Target != null)
            m_Movement.LookAt(Target.transform.position);
    }
}

}   // end namespace Game