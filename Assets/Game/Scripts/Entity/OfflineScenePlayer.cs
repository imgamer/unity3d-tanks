﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{

public class OfflineScenePlayer : OfflineSceneAvatar
{
    private TankMovement m_Movement;
    private TankShooting m_shooting;

    public override void Init()
    {
        m_Movement = this.gameObject.AddComponent<TankMovement>();
        m_shooting = this.gameObject.AddComponent<TankShooting>();
    }

    public override void EnableControl()
    {
        base.EnableControl();

        m_Movement.enabled = true;
        m_shooting.enabled = true;
    }

    public override void DisableControl()
    {
        base.DisableControl();

        m_Movement.enabled = false;
        m_shooting.enabled = false;
    }

}

}   // end namespace Game
