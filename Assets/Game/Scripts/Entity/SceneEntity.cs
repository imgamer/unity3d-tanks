﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using GameCommon;

namespace Game
{

public class SceneEntity : MonoBehaviour 
{

    protected int m_id;
    public int ID { get { return m_id; } }

    private Color m_color;
    public Color ModelColor
    {
        get { return m_color; }
        set { m_color = value; }
    }

    // 与Awake同步，用来初始化一些数据
    public virtual void Init(SceneEntityCreateParam p_param)
    {
        GameLogger.Log("SceneEntity({0}):Init.", p_param.m_id);
        m_id = p_param.m_id;
        ModelColor = p_param.m_color;

        SetColor();
    }

    private void Start()
    {
        EnterScene();
    }

    public virtual void OnDeath()
    { }

    private void OnDestroy()
    {
        LeaveScene();
    }

    protected virtual void EnterScene()
    { }

    protected virtual void LeaveScene()
    {
        WorldManager.Instance.CurrentScene.OnSceneEntityLeave(this);
    }

    protected void SetColor()
    {
        MeshRenderer[] renderers = this.GetComponentsInChildren<MeshRenderer>();

        for (int i = 0; i < renderers.Length; i++)
        {
            renderers[i].material.color = ModelColor;
        }
    }
}

}   // end namespace Game