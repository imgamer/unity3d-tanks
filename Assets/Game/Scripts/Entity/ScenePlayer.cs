﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{

public class ScenePlayer : SceneAvatar 
{
    public override void Init(SceneEntityCreateParam p_param)
    {
        base.Init(p_param);
        this.gameObject.AddComponent<TankMovement>();
        this.gameObject.AddComponent<TankShooting>();
    }

}

}   // end namespace Game