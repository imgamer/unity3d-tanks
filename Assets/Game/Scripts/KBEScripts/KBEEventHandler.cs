﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using GameCommon;

/// <summary>
/// 用于处理KBEngine的事件通知
/// </summary>
public class KBEEventHandler : Singleton<KBEEventHandler>
{
    protected override void OnInit()
    {
        base.OnInit();

        RegisterKBEEvent();
    }

    ~KBEEventHandler()
    { }

    private void RegisterKBEEvent()
    {
        KBEngine.Event.registerOut("onDisableConnect", this, "onDisableConnect");
        KBEngine.Event.registerOut("onLoginFailed", this, "onLoginFailed");
    }

    public void onDisableConnect()
    {
        GameLogger.Log("KBEEventProc::onDisableConnect");
    }

    public void onLoginFailed(UInt16 failedcode)
    {
        GameLogger.Error("KBEEventProc::onDisableConnect:failedcode({0}),errstr({1}).", failedcode, GetServerErrDescr(failedcode));
    }

    private string GetServerErrDescr(UInt16 id)
    {
        KBEngine.KBEngineApp.ServerErr err;
        KBEngine.KBEngineApp.serverErrs.TryGetValue(id, out err);
        return err.descr;
    }
}
