﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using GameCommon;

namespace Game
{

public class Main : MonoBehaviour 
{
    private static Main m_instance;

    void Awake()
    {

        if(m_instance == null)
        {
            m_instance = this;
        }
        else if (m_instance != this)
        {
            Debug.LogError("Main::Awake:duplicate Main instance, now destoy the newer one.");
            Destroy(this.gameObject);
        }
    }

	// Use this for initialization
	void Start () 
    {
        DontDestroyOnLoad(this.gameObject);

        InitSingleton();
        InitConfig();

        WorldManager.Instance.LoginScene();

        Test();
	}
	
    private void InitSingleton()
    {
        SceneEntityManager.Create();
    }

    private void InitConfig()
    {
        // 离线entity配置
        //OfflineEntityDataLoader.Create();
    }

    /// <summary>
    /// 用于临时测试
    /// </summary>
    private void Test()
    {
        //TestLoadResource();

        TestLoadConfig();
    }

    // for test
    private void TestLoadResource()
    {
        Instantiate(ResourcesLoader.Instance.Load("Prefabs/DustTrail"));

        ResourcesLoader.Instance.LoadAsync("Prefabs/PumpJack", delegate(GameObject p_gameObject)
        {
            GameLogger.Log("Test:LoadAsync gameObject({0}).", p_gameObject.name);
            Instantiate(p_gameObject);
        }
            );
    }

    // for test
    private void TestLoadConfig()
    {
        OfflineEntityData data1 =  OfflineEntityDataLoader.Instance.GetData(1);
        OfflineEntityData data2 = OfflineEntityDataLoader.Instance.GetData(222);

        if(data1 != null)
            GameLogger.Log("Main::TestLoadConfig:load OfflineEntityData data1 modelColor:" + ColorUtil.HexToColor(data1.modelColor));

        if (data2 != null)
            GameLogger.Log("Main::TestLoadConfig:load OfflineEntityData data2:" + data2.utype);
    }
}

}   // end namespace Game