using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

using GameCommon;

namespace Game
{

/// <summary>
/// 基础场景类，所有的加载场景都来源于此
/// </summary>
public class BaseScene : ScriptScene
{
    protected string m_sceneName;     // 场景路径

    private List<SceneEntity> m_Players = new List<SceneEntity>();
    public List<SceneEntity> Players
    {
        get { return m_Players; }
    }
    private List<SceneEntity> m_Monsters = new List<SceneEntity>();
    public List<SceneEntity> Monsters
    {
        get { return m_Monsters; }
    }

    public override void Enter()
    {

        SceneManager.LoadSceneAsync(m_sceneName); // 默认异步加载场景
        // 所有场景异步加载完毕都会回调这个函数，在回调后要注销
        // 暂时用这个回调机制，以后研究是否可以改为传入回调函数
        SceneManager.sceneLoaded += SceneManager_sceneLoaded;

        // TODO:显示加载进度条界面，
        // 可以是通知加载管理器，加载管理器根据需要加载的资源来逐项加载并按进度做进度条表现
        // 因为需要预先加载一些资源，可以在这个场景中给加载管理器添加任务
    }

    void SceneManager_sceneLoaded(Scene p_scene, LoadSceneMode p_loadSceneMode)
    {
        SceneManager.sceneLoaded -= SceneManager_sceneLoaded;

        this.OnSceneLoaded(p_scene, p_loadSceneMode);
        WorldManager.Instance.OnEnterSceneSuccess();

    }

    public override void Leave()
    {
        m_Players.Clear();
        m_Monsters.Clear();
    }

    public override void Init(object p_data)
    {
        //throw new System.NotImplementedException();
    }

    protected virtual void OnSceneLoaded(Scene p_scene, LoadSceneMode p_loadSceneMode)
    { }

    public override void OnSceneEntityEnter(SceneEntity p_entity)
    {
        base.OnSceneEntityEnter(p_entity);

        SceneEntity player = SceneEntityManager.Instance.Player;
        if (player != null && p_entity.ID == player.ID)
            m_Players.Add(p_entity);
        else
        {
            m_Monsters.Add(p_entity);
        }

        GameLogger.Log("OnSceneEntityEnter:monster({0}),player({1})", m_Monsters.Count, m_Players.Count);
    }
}

}   // end namespace Game

