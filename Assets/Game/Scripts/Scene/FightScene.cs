﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

using GameCommon;

namespace Game
{

public class FightScene : BaseScene
{
    public override void Init(object p_data)
    {
        base.Init(p_data);
        m_sceneName = "YellowBase";
    }

    public override void Enter()
    {
        base.Enter();
        
    }

    public override void Leave()
    {
        UIManager.Instance.CloseUI(UIDefine.UIPath_Joystick);
        base.Leave();
    }

    protected override void OnSceneLoaded(Scene p_scene, LoadSceneMode p_loadSceneMode)
    {
        GameLogger.Log("FightScene:OnSceneLoaded:{0}.", p_scene.name);

        base.OnSceneLoaded(p_scene, p_loadSceneMode);
        UIManager.Instance.OpenUI(UIDefine.UIPath_Joystick, true);
    }

}

}   // end namespace Game