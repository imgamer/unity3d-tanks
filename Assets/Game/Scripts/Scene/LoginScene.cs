﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using GameCommon;

namespace Game
{
    class LoginScene: ScriptScene
    {
        public override void Init(object p_data)
        {
            //throw new NotImplementedException();
        }

        public override void Enter()
        {
            GameLogger.Log("LoginScene::Enter");
            UIManager.Instance.OpenUI(UIDefine.UIPath_Login, true);
        }

        public override void Leave()
        {
            UIManager.Instance.CloseUI(UIDefine.UIPath_Login);
        }
    }


}   // end namespace Game