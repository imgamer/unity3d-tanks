using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using GameCommon;

namespace Game
{

/// <summary>
/// 通知spawnPoint开始创建
/// </summary>
public class LevelSpawnPoint : LevelElement
{
    public Color m_Color;
    public OfflineEntityUtype m_Type;

    private OfflineSceneAvatar m_SceneEntity = null;

    protected override void OnActive()
    {
        base.OnActive();

        Spawn();

        Pass();
    }

    protected override void OnReset()
    {
        base.OnReset();

        if (m_SceneEntity)
            m_SceneEntity.Reset();
    }

    public OfflineSceneAvatar Spawn()
    {
        if (m_SceneEntity == null)
        {
            //SceneEntityCreateParam createParam = new SceneEntityCreateParam();
            //createParam.m_color = m_Color;
            //m_SceneEntity = SceneEntityManager.Instance.CreateSceneEntity(m_Type, transform.position, transform.rotation, createParam) as SceneOfflineAvatar;
            m_SceneEntity = SceneEntityManager.Instance.CreateOfflineSceneEntity(m_Type, transform.position, transform.rotation, m_Color);
            m_SceneEntity.SpawnPoint = this;
        }
        else
        {
            m_SceneEntity.Reset();
        }

        return m_SceneEntity;
    }

    public void OnSceneEntityDestroy()
    {
        GameLogger.Log("LevelSpawnPoint::OnTankDestroy:" + m_SceneEntity.ID);
        m_SceneEntity = null;
        //TrySpawn();   不可以在销毁是创建entity，Error:Some objects were not cleaned up when closing the scene. (Did you spawn new GameObjects from OnDestroy?
    }
}

}   // end namespace Game
