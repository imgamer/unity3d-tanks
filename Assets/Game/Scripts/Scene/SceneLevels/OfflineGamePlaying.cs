﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{

enum OfflineFightResult
{
    None,
    Player,
    Monster,
}

public class OfflineGamePlaying : LevelElement
{
    public LevelDesignRoot m_levelDesignRoot;
    public float m_StartDelay = 3f;
    public float m_EndDelay = 3f;             

    private OfflineFightScene m_CurrentScene;
    private MessageCanvas m_MessageCanvas;

    private WaitForSeconds m_StartWait;
    private WaitForSeconds m_EndWait;

    private OfflineFightResult m_FightResult = OfflineFightResult.None;

    protected override void OnActive()
    {
        base.OnActive();

        m_CurrentScene = WorldManager.Instance.CurrentScene as OfflineFightScene;
        m_MessageCanvas = (MessageCanvas)UIManager.Instance.OpenUI(UIDefine.UIPath_Meassage, true);

        m_StartWait = new WaitForSeconds(m_StartDelay);
        m_EndWait = new WaitForSeconds(m_EndDelay);

        EnableTankControl();

        StartCoroutine(RoundPlaying());
    }

    private IEnumerator RoundPlaying()
    {
        // 1
        //m_MessageCanvas.SetText("敌人来袭！");
        yield return m_StartWait;

        // 2 
        m_MessageCanvas.SetText(string.Empty);
        EnableTankControl();
        CameraControl.Instance.SetCameraType(CameraType.PLAYER);

        while (!GameResult())
        {
            yield return null;
        }

        // 3
        DisableTankControl();
        m_MessageCanvas.SetText(EndMessage());
        yield return m_EndWait;

        // 4
        m_MessageCanvas.OnPlayerAgainButton += Button_PlayAgain;
        m_MessageCanvas.ShowSelect(true);
    }

    private void Button_PlayAgain()
    {
        m_MessageCanvas.SetText(string.Empty);
        m_MessageCanvas.ShowSelect(false);
        m_FightResult = OfflineFightResult.None;

        m_levelDesignRoot.Reset();
    }

    private void EnableTankControl()
    {
        foreach (OfflineScenePlayer entity in m_CurrentScene.Players)
        {
            entity.EnableControl();
        }
    }

    private void DisableTankControl()
    {
        foreach (OfflineScenePlayer entity in m_CurrentScene.Players)
        {
            entity.DisableControl();
        }
    }

    private bool GameResult()
    {
        int monsterTankLeft = 0;

        foreach (OfflineSceneAvatar entity in m_CurrentScene.Monsters)
        {
            if (!entity.IsDead())
                monsterTankLeft++;
        }
        if (monsterTankLeft <= 0)
            m_FightResult = OfflineFightResult.Player;
        else if (SceneEntityManager.Instance.OfflinePlayer.IsDead())
            m_FightResult = OfflineFightResult.Monster;

        return m_FightResult != OfflineFightResult.None;
    }

    private string EndMessage()
    {
        string message = "DRAW!";

        message += "\n\n\n\n";

        OfflineScenePlayer player = SceneEntityManager.Instance.OfflinePlayer;

        string winMessage = " WINS THE GAME!";
        if (m_FightResult == OfflineFightResult.Monster)
            winMessage = " LOST THE GAME!";

        string playerColorText = "<color=#" + ColorUtility.ToHtmlStringRGB(player.ModelColor) + ">" + player.name + "</color>" + winMessage;

        return playerColorText;
    }
}

}   // end namespace Game