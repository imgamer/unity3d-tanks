﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{

public class OfflineGameStarting : LevelElement
{
    protected override void OnActive()
    {
        base.OnActive();

        // 需要保证OfflinePlayer已经创建
        OfflineScenePlayer player = SceneEntityManager.Instance.OfflinePlayer;
        player.name = "大魔王";

        MessageCanvas messageCanvas = (MessageCanvas)UIManager.Instance.OpenUI(UIDefine.UIPath_Meassage, true);
        messageCanvas.SetText("敌人来袭！");

        DisableTankControl();

        Pass();
    }

    private void DisableTankControl()
    {
        OfflineFightScene currentScene = WorldManager.Instance.CurrentScene as OfflineFightScene;
        foreach (OfflineScenePlayer entity in currentScene.Players)
        {
            entity.DisableControl();
        }
    }
}

}   // end namespace Game