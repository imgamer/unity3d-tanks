﻿using System;
using UnityEngine.SceneManagement;

using GameCommon;

namespace Game
{


/// <summary>
/// 初始场景，对应入口场景
/// </summary>
public class StartScene : ScriptScene
{
    public StartScene()
	{
	}

    public override void Enter()
    {
        GameLogger.Log("StartScene::Enter");
        WorldManager.Instance.EnterScene(Define.SCENE_SCRIPT_NAME_LOGIN);
        //UIManager.Instance.OpenUI(UIDefine.UIPath_Login, true);
    }

    public override void Leave()
    {
        //UIManager.Instance.CloseUI(UIDefine.UIPath_Login);
    }

    public override void Init(object p_data)
    {
        //throw new NotImplementedException();
    }
}

}   // end namespace Game