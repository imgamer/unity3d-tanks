﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameCommon;


namespace Game
{

/// <summary>
/// 场景脚本负责维护场景配置数据。
/// 1. 存在服务端的情况下，服务端的空间对应客户端的场景，服务端会有空间静态
/// 数据的配置，服务端会先进入空间，然后通知客户端。客户端使用服务端发送
/// 过来的场景id，读取客户端的场景配置，加载相应的场景，甚至配置在其中的
/// 关卡（stage）规则或者一些表现数据。
/// 也就是说，客户端会创建这个Scene脚本，然后再创建unity场景并进入，此时
/// 没有场景，可以缓存服务端发送过来的空间中的entity数据，等待场景加载完
/// 毕再在场景中创建这些entity的GameObject。
/// 场景脚本使得这些事情变得简单。
/// 
/// 2. 对于unity来说，也可以通过配置每个场景prefab来达到目的，不是加载json
/// 配置，而是在场景中实例化prefab来控制流程。这个想法以后再深入考虑。
/// 
/// 3. 在没有服务端的情况下，也可以有场景脚本的管理流程，维护一些配置的静
/// 态数据，方便以后扩展。
/// </summary>
public abstract class ScriptScene
{
    // 每个场景都有自己的SceneEntity列表
    private List<SceneEntity> m_SceneEntities = new List<SceneEntity>();
    public List<SceneEntity> SceneEntities
    {
        get { return m_SceneEntities; }
    }

    public virtual void OnSceneEntityEnter(SceneEntity p_entity)
    {
        m_SceneEntities.Add(p_entity);
    }

    public virtual void OnSceneEntityLeave(SceneEntity p_entity)
    {
        m_SceneEntities.Remove(p_entity);
    }

    public abstract void Enter();
    public abstract void Leave();

    /// <summary>
    ///  反射的创建方式无法调用带有参数的构造函数
    ///  创建完毕后通过此接口初始化场景实例
    /// </summary>
    /// <param name="p_data"></param>
    public abstract void Init(object p_data);
}

/// <summary>
/// 场景脚本的管理。
/// </summary>
public class WorldManager : Singleton<WorldManager>
{
    // 需要考虑的问题：m_CurrentScene是ScriptScene，这意味着需要给外部提供通用
    // 的接口，但场景脚本的行为有时差异过大，如在战斗场景，需要有不同的SceneEntity
    // 分类，这些行为如果放到基类就太冗余了，需要更好的设计来达到这个目的
    // 或许不该提供当前场景属性，或者当前场景不该维护差异很大的数据和行为。
    // 或者外部拿到当前场景，根据需求转换为所需的场景类型，这就和面对接口编程相悖
    private ScriptScene m_CurrentScene;
    public ScriptScene CurrentScene
    {
        get { return m_CurrentScene; }
    }

    public bool IsScenceAvailable
    {
        get;
        set;
    }

    /// <summary>
    /// 进入初始场景，引擎自动加载的场景，和需要手动加载的场景不一样，先进入此场景才加载场景脚本，开始游戏进程
    /// </summary>
    public void LoginScene()
    {
        StartScene scene = new StartScene();
        scene.Init(null);
        m_CurrentScene = scene;
        scene.Enter();
    }

    /// <summary>
    /// 进入场景
    /// </summary>
    /// <param name="p_scenceName">场景名字，以后对接服务端可以改成证型id，和服务端统一的场景配置唯一索引</param>
    public void EnterScene(string p_scenceName)
    {
        Type sceneClass = Type.GetType("Game." + p_scenceName);
        ScriptScene scenceScript = (ScriptScene)Activator.CreateInstance(sceneClass);
        if(scenceScript == null)
        {
            Debug.LogError("SceneScriptManager::EnterScene:cant find sence script:" + p_scenceName);
            return;
        }
        scenceScript.Init(null);

        if (m_CurrentScene != null)
        {
            m_CurrentScene.Leave();
            IsScenceAvailable = false;
        }
        m_CurrentScene = scenceScript;
        m_CurrentScene.Enter();
    }

    public void OnEnterSceneSuccess()
    {
        Debug.Log("SceneScriptManager::OnEnterSuccess.");
        IsScenceAvailable = true;

        // TODO:创建缓存数据中的GameObject

        EventManager.Fire<ScriptScene>(EventID.SCENCE_ENTER_SUCCESS, m_CurrentScene);
    }

}

}   // end namespace Game
