﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{

public class TankCanvas : MonoBehaviour 
{
    public Slider m_AimSlider;

    // HP显示相关
    public Slider m_HealthSlider;
    public Image m_HealthImage;
    public Color m_FullHealthColor = Color.green;
    public Color m_ZeroHealthColor = Color.red;
    // 最大HP值，根据此值来表现血条，因为slider的value范围为100，需要把当前的HP值插值到此范围
    public float m_HealthMax = 100f;

    public void SetAimSlider(float p_value)
    {
        m_AimSlider.value = p_value;
    }

    public void SetHealthUI(float p_currentHealthValue)
    {
        m_HealthSlider.value = p_currentHealthValue;
        m_HealthImage.color = Color.Lerp(m_ZeroHealthColor, m_FullHealthColor, p_currentHealthValue / m_HealthMax);
    }
}

}   // end namespace Game