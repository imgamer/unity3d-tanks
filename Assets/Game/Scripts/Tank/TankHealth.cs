﻿using UnityEngine;
using UnityEngine.UI;

namespace Game
{

[RequireComponent(typeof(SceneEntity))]
public class TankHealth : MonoBehaviour
{
    private GameObject m_ExplosionPrefab;
    private AudioSource m_ExplosionAudio;          
    private ParticleSystem m_ExplosionParticles;

    public float m_StartingHealth = 100f;          
    private float m_CurrentHealth;  
    private bool m_Dead;
    private TankCanvas m_TankCanvas;
    private SceneEntity m_SceneEntity;

    private void Awake()
    {
        m_TankCanvas = transform.Find("Canvas").GetComponent<TankCanvas>();

        m_ExplosionPrefab = ResourcesLoader.Instance.Load(AssetsDefine.TANK_EXPLOSION_PATH);
        m_ExplosionParticles = Instantiate(m_ExplosionPrefab).GetComponent<ParticleSystem>();
        m_ExplosionAudio = m_ExplosionParticles.GetComponent<AudioSource>();

        m_ExplosionParticles.gameObject.SetActive(false);
    }

    private void Start()
    {
        // 在Start中获得动态添加的SceneEntity组件
        m_SceneEntity = GetComponent<SceneEntity>();
    }

    private void OnEnable()
    {
        m_CurrentHealth = m_StartingHealth;
        m_Dead = false;

        SetHealthUI();
    }
    

    public void TakeDamage(float amount)
    {
        // Adjust the tank's current health, update the UI based on the new health and check whether or not the tank is dead.
        m_CurrentHealth -= amount;

        SetHealthUI();

        if(m_CurrentHealth < 0.0f && !m_Dead)
            OnDeath();
    }


    private void SetHealthUI()
    {
        // Adjust the value and colour of the slider.
        m_TankCanvas.SetHealthUI(m_CurrentHealth);
    }


    private void OnDeath()
    {
        // Play the effects for the death of the tank and deactivate it.
        m_Dead = true;

        m_ExplosionParticles.transform.position = transform.position;
        m_ExplosionParticles.gameObject.SetActive(true);
        m_ExplosionParticles.Play();

        m_ExplosionAudio.Play();
        
        m_SceneEntity.OnDeath();
    }
}

}   // end namespace Game