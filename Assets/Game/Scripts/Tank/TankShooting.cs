﻿using UnityEngine;
using UnityEngine.UI;

using UnityStandardAssets.CrossPlatformInput;

namespace Game
{


public class TankShooting : MonoBehaviour
{
    // 这些属性都应该通过json配置
    
    public float m_MinLaunchForce = 15f; 
    public float m_MaxLaunchForce = 30f; 
    public float m_MaxChargeTime = 0.75f;

    private Rigidbody m_Shell;

    private int m_PlayerNumber = 1;    
    private string m_FireButton;         
    private float m_CurrentLaunchForce;
    private float m_ChargeSpeed;         // 发射力的增长速度，取决于最大蓄力时间。(charge time,蓄力时间)
    private bool m_Fired;

    private TankAudioHandle m_AudioHandle;
    private TankCanvas m_TankCanvas;
    private Transform m_FireTransform;    

    private void Awake()
    {
        m_AudioHandle = GetComponent<TankAudioHandle>();
        m_TankCanvas = transform.Find("Canvas").GetComponent<TankCanvas>();
        m_FireTransform = transform.Find("FireTransform");

        GameObject shellPrefab = ResourcesLoader.Instance.Load(AssetsDefine.SHELL_PREFAB_PATH);
        m_Shell = shellPrefab.GetComponent<Rigidbody>();
    }

    private void OnEnable()
    {
        m_CurrentLaunchForce = m_MinLaunchForce;
        m_TankCanvas.SetAimSlider(m_MinLaunchForce);
    }

    private void Start()
    {
        m_FireButton = "Fire" + m_PlayerNumber;

        // The rate that the launch force charges up is the range of possible forces by the max charge time.
        // 发射蓄力率是最大蓄力时间蓄力能达到的可能范围。(charge up，充电、蓄力)。
        m_ChargeSpeed = (m_MaxLaunchForce - m_MinLaunchForce) / m_MaxChargeTime;
    }

    private void Update()
    {

        // Track the current state of the fire button and make decisions based on the current launch force.
        // 先检查是否满足开火条件
        if(m_CurrentLaunchForce > m_MaxLaunchForce && !m_Fired)
        {
            m_CurrentLaunchForce = m_MaxLaunchForce;
            Fire();
        }
        // 第一次按下开火键
		else if(CrossPlatformInputManager.GetButtonDown(m_FireButton))
        {
            m_Fired = false;
            m_CurrentLaunchForce = m_MinLaunchForce;

            // 开始播炮弹滑膛的声音
            m_AudioHandle.PlayShootingAudio(ShootingAudioType.Charging);
        }
        // 开火键按下状态，进行蓄力和表现
        else if (CrossPlatformInputManager.GetButton(m_FireButton) && !m_Fired)
        {
            m_CurrentLaunchForce += m_ChargeSpeed * Time.deltaTime;

            m_TankCanvas.SetAimSlider(m_CurrentLaunchForce);
        }
		else if(CrossPlatformInputManager.GetButtonUp(m_FireButton) && !m_Fired)
        {
            Fire();
        }
    }

    private void Fire()
    {
        // Instantiate and launch the shell.

        m_Fired = true;

        // Instantiate的对象也可以是个组件，这里m_Shell必须是Rigidbody，否则无法转换为Rigidbody。可了解Instantiate的重载方法
        Rigidbody shellInstance = Instantiate(m_Shell, m_FireTransform.position, m_FireTransform.rotation) as Rigidbody;
        shellInstance.velocity = m_CurrentLaunchForce * m_FireTransform.forward;

        m_AudioHandle.PlayShootingAudio(ShootingAudioType.Fire);

        m_CurrentLaunchForce = m_MinLaunchForce;
        m_TankCanvas.SetAimSlider(m_MinLaunchForce);
    }
}

}   // end namespace Game