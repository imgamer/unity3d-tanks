﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
	
public abstract class UIWindow : MonoBehaviour 
{
    protected bool m_isInited = false;

	public void Init(Camera p_uiCamera)
    {
        if (!m_isInited)
        {
			Canvas winCanvas = this.GetComponent<Canvas> ();
			winCanvas.renderMode = RenderMode.ScreenSpaceCamera;
			winCanvas.worldCamera = p_uiCamera;
			m_isInited = true;
            OnInit();
            
        }
    }

    public void Open()
    {
        if (!gameObject.activeSelf) 
            gameObject.SetActive(true);
        OnOpen();
    }

    public void Close()
    {
        OnClose();
        if (gameObject.activeSelf)
            gameObject.SetActive(false);
    }

    protected virtual void OnInit()
    {}
    protected virtual void OnClose()
    {}
    protected virtual void OnOpen()
    {}
}

}	// end namespace GameUI