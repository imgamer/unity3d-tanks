using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace UnityStandardAssets.CrossPlatformInput
{

public class Joystick : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler
{
	public enum AxisOption
	{
		// Options for which axes to use
		Both, // Use both
		OnlyHorizontal, // Only horizontal
		OnlyVertical // Only vertical
	}

	public float MovementRange = 100;
	public AxisOption axesToUse = AxisOption.Both; // The options for the axes that the still will use
	public string horizontalAxisName = "Horizontal"; // The name given to the horizontal axis for the cross platform input
	public string verticalAxisName = "Vertical"; // The name given to the vertical axis for the cross platform input

	Vector3 m_StartPos;
	bool m_UseX; // Toggle for using the x axis
	bool m_UseY; // Toggle for using the Y axis
	CrossPlatformInputManager.VirtualAxis m_HorizontalVirtualAxis; // Reference to the joystick in the cross platform input
	CrossPlatformInputManager.VirtualAxis m_VerticalVirtualAxis; // Reference to the joystick in the cross platform input

    private RectTransform m_parentRectTransform;
    private RectTransform m_selfRectTransform;
    private Vector2 m_startRectPos;

	void OnEnable()
	{
		CreateVirtualAxes();
	}

    void Start()
    {
        m_StartPos = transform.position;

        m_selfRectTransform = transform as RectTransform;
        m_parentRectTransform = transform.parent.GetComponent<RectTransform>();
        // assert m_parentRectTransform != null;

        m_startRectPos = m_selfRectTransform.anchoredPosition;
    }

	void CreateVirtualAxes()
	{
		// set axes to use
		m_UseX = (axesToUse == AxisOption.Both || axesToUse == AxisOption.OnlyHorizontal);
		m_UseY = (axesToUse == AxisOption.Both || axesToUse == AxisOption.OnlyVertical);

		// create new axes based on axes to use
		if (m_UseX)
		{
			m_HorizontalVirtualAxis = new CrossPlatformInputManager.VirtualAxis(horizontalAxisName);
			CrossPlatformInputManager.RegisterVirtualAxis(m_HorizontalVirtualAxis);
		}
		if (m_UseY)
		{
			m_VerticalVirtualAxis = new CrossPlatformInputManager.VirtualAxis(verticalAxisName);
			CrossPlatformInputManager.RegisterVirtualAxis(m_VerticalVirtualAxis);
		}
	}

    void UpdateVirtualAxes(Vector2 p_anchorPos)
    {
        var delta = p_anchorPos - m_startRectPos;
        delta /= MovementRange;

        m_HorizontalVirtualAxis.Update(delta.x);
        m_VerticalVirtualAxis.Update(delta.y);
    }

    public void OnDrag(PointerEventData data)
    {
        // data是屏幕坐标，在overlay模式下和世界坐标的xy一致，但camera模式下需要转为手柄的anchor坐标，这个坐标和起始坐标在同一个坐标空间
        Vector2 newPoint;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(m_parentRectTransform, data.position, data.pressEventCamera, out newPoint);

        Vector2 deltaVec = newPoint - m_startRectPos;                                           // 实际拖动的位移向量
        float range = Mathf.Clamp(deltaVec.magnitude, -MovementRange, MovementRange);           // 限制移动的距离
        m_selfRectTransform.anchoredPosition = m_startRectPos + range * deltaVec.normalized;    // 得出正确位置

        UpdateVirtualAxes(m_selfRectTransform.anchoredPosition);
    }
    public void OnPointerUp(PointerEventData data)
    {
        m_selfRectTransform.anchoredPosition = m_startRectPos;
        UpdateVirtualAxes(m_startRectPos);
    }

    public void OnPointerDown(PointerEventData data)
    {
        // 注意：不可以在这里记录初始位置，按钮区域很大，点击哪个位置是不确定的，会造成不确定的起始点。
        // 记录在父对象变换中的初始位置，要根据此位置做偏移
        //RectTransformUtility.ScreenPointToLocalPointInRectangle(m_parentRectTransform, data.position, data.pressEventCamera, out m_startRectPos);
    }

	void OnDisable()
	{
        //// remove the joysticks from the cross platform input
        if (m_UseX)
        {
            m_HorizontalVirtualAxis.Remove();
        }
        if (m_UseY)
        {
            m_VerticalVirtualAxis.Remove();
        }
	}


    #region 废弃代码：在overlay模式下的拖动控制
    void UpdateVirtualAxes(Vector3 value)
    {
        var delta = m_StartPos - value;
        delta.y = -delta.y;         // 屏幕坐标以左上角为原点，向下时Y正轴，与unity3d的坐标系相反
        delta /= MovementRange;     // 虚拟轴的偏移值应该在[-1, 1]区间
        if (m_UseX)
        {
            m_HorizontalVirtualAxis.Update(-delta.x);
        }

        if (m_UseY)
        {
            m_VerticalVirtualAxis.Update(delta.y);
        }
    }

    public void OnDrag1(PointerEventData data)
    {
        Vector3 newPos = Vector3.zero;

        if (m_UseX)
        {
            int delta = (int)(data.position.x - m_StartPos.x);
            //delta = Mathf.Clamp(delta, - MovementRange, MovementRange);   // 限制偏移量在[-MovementRange, MovementRange]区间，这会是个矩形的区域
            newPos.x = delta;
        }

        if (m_UseY)
        {
            int delta = (int)(data.position.y - m_StartPos.y);
            //delta = Mathf.Clamp(delta, -MovementRange, MovementRange);
            newPos.y = delta;
        }

        // 限制操纵杆在以m_StartPos为圆心半径不超过MovementRange的范围移动
        // 算法是计算出偏移向量，保持朝向不变，大小不超过MovementRange
        Vector3 dragPos = new Vector3(m_StartPos.x + newPos.x, m_StartPos.y + newPos.y, m_StartPos.z + newPos.z);   // 得到拖动点
        Vector3 moveVector = dragPos - m_StartPos;      // 计算实际偏移向量
        float range = Mathf.Clamp((int)moveVector.magnitude, -MovementRange, MovementRange);  // 限制偏移向量在MovementRange半径
        transform.position = m_StartPos + (range * moveVector.normalized); // 计算出最终位置

        UpdateVirtualAxes(transform.position);
    }


    public void OnPointerUp1(PointerEventData data)
    {
        transform.position = m_StartPos;
        UpdateVirtualAxes(m_StartPos);
    }
    #endregion
}

}   // end namespace UnityStandardAssets.CrossPlatformInput