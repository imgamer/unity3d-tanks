﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityStandardAssets.CrossPlatformInput;


namespace Game
{

public class JoystickWin : UIWindow
{
		// 在此处维护虚拟按钮，创建、注册、使用
		public string m_fireButtonName = "Fire1";
		private CrossPlatformInputManager.VirtualButton m_virtalButton;

		public void Button_FireDown()
		{
			CrossPlatformInputManager.SetButtonDown (m_fireButtonName);
		}

		public void Button_FireUp()
		{
			CrossPlatformInputManager.SetButtonUp (m_fireButtonName);
		}

		void OnEnable()
		{
			CreateVirtualBottons ();
		}

		void OnDisable()
		{
			m_virtalButton.Remove ();
		}

		private void CreateVirtualBottons()
		{
			m_virtalButton = new CrossPlatformInputManager.VirtualButton (m_fireButtonName);
			CrossPlatformInputManager.RegisterVirtualButton (m_virtalButton);
		}
}


}	// end namespace GameUI
