﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{



public class LoginWin : UIWindow
{

    protected override void OnInit()
    {

    }

    public void Button_login(string p_sceneClassName)
    {
        WorldManager.Instance.EnterScene( p_sceneClassName);
    }

}

}   // end namespace Game