﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Game
{

public delegate void MessageCanvasCallback();

public class MessageCanvas : UIWindow
{
    
    public GameObject m_SelectPanel;
    private Text m_text;

    public event MessageCanvasCallback OnPlayerAgainButton;

    protected override void OnInit()
    {
        base.OnInit();

        m_text = transform.Find("Text").GetComponent<Text>();
    }

    public void SetText(string p_text)
    {
        m_text.text = p_text;
    }

    public void ShowSelect(bool p_active)
    {
        m_SelectPanel.SetActive(p_active);
    }

    public void Button_PlayerAgain()
    {
        //GameLogger.Log("MessageCanvas::Button_PlayerAgain.");

        if (OnPlayerAgainButton != null)
            OnPlayerAgainButton();
        
        //GameManager.Instance.ReStart();
    }

    public void Button_Quit()
    {
        //GameLogger.Log("MessageCanvas::Button_Quit.");
        Application.Quit();
    }

    public void Button_CameraSwitchDown()
    {
        CameraControl.Instance.SetCameraType(CameraType.ALL_ENTITY);
    }

    public void Button_CameraSwitchUp()
    {
        CameraControl.Instance.SetCameraType(CameraType.PLAYER);
    }
}

}   // end namespace GameUI