by wangshufeng

### 版本信息  
版本：0.9.0
来自：https://github.com/lbv/litjson
官方网站：http://lbv.github.io/litjson/

### 导入方式
2种方式都可以，目前使用2：
1. 把编译后的dll放到plugins目录，然后就能using了
2. 直接把下载的源码中的src目录放到plugins目录

### 编译警告CLSCompliant(false)

编译后会提示如下警告，可以忽略。  
Assets/Plugins/LitJson/JsonWriter.cs(368,10): warning CS3021: `LitJson.JsonWriter.Write(ulong)' does not need a CLSCompliant attribute because the assembly is not marked as CLS-compliant
"由于程序集没有 CLSCompliant 特性，因此“LitJson.JsonWriter.Write(ulong)”不需要 CLSCompliant 特性"
CLSCompliant(false)描述了方法是否遵守CLS标准，这样才能被其它.net语言使用。
见：https://stackoverflow.com/questions/570452/what-is-the-clscompliant-attribute-in-net

### 使用注意事项（网络收集，未验证，可能是旧版问题）：
1. 在使用JsonMapper.ToObject<>时，模板与json表字段的名称是一样的。
2. 若包含Dictionary结构，则key的类型必须是string，而不能是int类型（如需表示id等），否则无法正确解析。
3. 若需要小数，要使用double类型，而不能使用float，可后期在代码里再显式转换为float类型。

